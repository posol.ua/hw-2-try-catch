/*Теоретичне питання
Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

try...catch можна використовувати для роботи з зовнішніми даними, з мережею, з файлами, з DOM. 
Конструкція try...catch допомагає зробити код більш стійким до помилок і дозволяє їх обробляти 
без аварійного завершення програми.

Завдання
Дано масив books.
Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). 
Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.*/

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

const root = document.getElementById('root');
const ul = document.createElement('ul');
root.append(ul);

books.forEach((book) => {
    try {
        if (!book.author) {
            throw new Error (`Властивості автор не має в об'єкті`);
        }
        if (!book.name) {
            throw new Error (`Властивості назви не має в об'єкті`);
        }
        if (!book.price) {
            throw new Error (`Властивості ціни не має в об'єкті`);
        }
        const li = document.createElement('li');
        li.textContent = `${book.author}, ${book.name}, ${book.price}`;
        ul.append(li);
    } catch (error) {
        console.error(error.message);
    }
})
